﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CamTarget : MonoBehaviour
{
    Transform herder;
    Transform dog;

    float distZ = 0;
    float shift = 0.5f;

    private void Start() {
        herder = GameObject.FindWithTag("Herder").transform;
        dog = GameObject.FindWithTag("HerderDog").transform;
    }

    void Update()
    {
        const float maxDistance = 15f; // look at GameDirector.dogDistance.y 
        distZ = herder.position.z - dog.position.z;
        float t = Mathf.Abs(distZ / maxDistance);

        if (distZ < 0) {
            shift = Mathf.Lerp(0.5f, 0.425f, t);
        } else {
            shift = 0.5f;
        }

        Vector3 pos = new Vector3(
            (herder.position.x + dog.position.x) * 0.5f,
            Mathf.Lerp(0f, 0.5f, t),
            Mathf.Lerp(herder.position.z, dog.position.z, shift)
        );
        transform.position = pos;
    }
}
