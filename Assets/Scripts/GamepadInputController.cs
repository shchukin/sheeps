﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class GamepadInputController : MonoBehaviour, InputController
{
    public enum MainStick {Left, Right}

    public MainStick mainStick;

    StickControl moveDirectionControl = null;

    void Start() {
        Gamepad gamepad = Gamepad.current;
        if (gamepad == null) {
            return;
        }

        if (mainStick == MainStick.Left) {
            moveDirectionControl = Gamepad.current.leftStick;
        } else {
            moveDirectionControl = Gamepad.current.rightStick;
        }
    }

    public Vector2 MoveDirection() {
        if (moveDirectionControl != null) {
            Vector2 v = moveDirectionControl.ReadValue();
            if (v.magnitude < 0.1f) {
                return Vector2.zero;
            }

            return v;
        }

        return Vector2.zero;
    }

    public bool Fire() {
        Gamepad gamepad = Gamepad.current;
        if (gamepad == null) {
            return false;
        }

        if (mainStick == MainStick.Left) {
            return gamepad.leftTrigger.isPressed;
        } else {
            return gamepad.rightTrigger.isPressed;
        }
    }
}
