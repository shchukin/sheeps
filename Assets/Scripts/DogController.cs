﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DogController : MonoBehaviour
{
    public enum State {
        FreeRun,
        GoHerder
    }

    public Joystick joystick;
    public float maxSpeed = 3f;
    public float maxForce = 16f; // 20

    public Transform herder;
    //public float barkRate = 2f;

    //float nextFireTime = 0;
    State state = State.FreeRun;
    NavMeshAgent agent;
    Rigidbody body;
    float goHerderTimer;
    float goHerderTimeout = 0.25f;
    Locomotion locomotion;


    public State GetState() {
        return state;
    }

    void Start() {
        agent = GetComponent<NavMeshAgent>();
        body = GetComponent<Rigidbody>();

        IUserController userController = new JoystickController(joystick);
        locomotion = new Locomotion(maxSpeed, maxForce, body, userController);
    }

    void Update() {
        if (state == State.GoHerder) {
            goHerderTimer -= Time.deltaTime;
            if (goHerderTimer <= 0) {
                goHerderTimer += goHerderTimeout;
                agent.SetDestination(herder.position);
            }
            return;
        }

        locomotion.Update();
        updateBark();
    }

    public void SwitchState(State newstate) {
        if (state == newstate) {
            return;
        }

        state = newstate;

        switch (state) {
            case State.FreeRun:
                agent.enabled = false;
                body.isKinematic = false;
                break;
            case State.GoHerder:
                body.isKinematic = true;
                agent.enabled = true;
                agent.SetDestination(herder.position);
                goHerderTimer = goHerderTimeout;
                break;
        }
    }

    void updateBark() {
        //if (!inputController.Fire()) {
        //    return;
        //}

        //float t = Time.realtimeSinceStartup;
        //if (t < nextFireTime) {
        //    return;
        //}
        //nextFireTime = t + 1f / barkRate;

        //Debug.Log("BARK");
    }

}
