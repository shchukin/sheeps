﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sheep {
    public class SheepStateIdle : SheepState {
        float idleTimer;

        public override StateType Type => StateType.Idle;

        public SheepStateIdle(SheepStateMashine fsm, SheepMemory memory) : base(fsm, memory) { }

        public override void OnEnter() {
            mem.desiredWanderDirection = Vector3.zero;
            idleTimer = Random.Range(0.3f, 1.8f);
            // fsm.steering.SetState(SteeringLocomotion.State.Idle);
        }

        public override void DoIt(float deltaTime) {
            if (checkDanger()) {
                SwitchState(StateType.Flee);
                return;
            }

            idleTimer -= deltaTime;
            if (idleTimer <= 0) {
                SwitchState(StateType.Wander);
            }
        }
    }
}