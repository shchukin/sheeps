﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sheep {

    public class SheepStateTurn : SheepState {
        public override StateType Type => StateType.Turn;

        public SheepStateTurn(SheepStateMashine fsm, SheepMemory memory) : base(fsm, memory) { }

        public override void OnEnter() {
            //fsm.steering.SetState(SteeringLocomotion.State.Idle);
        }

        public override void DoIt(float deltaTime) {
            if (checkDanger()) {
                SwitchState(StateType.Flee);
                return;
            }

            //float rotSpeed = mem.rotSpeed * deltaTime;
            //mem.body.rotation = Quaternion.RotateTowards(mem.rotation, mem.desiredRot, rotSpeed);
            //float angle = Quaternion.Angle(mem.rotation, mem.desiredRot);
            //if (Mathf.Abs(angle) <= rotSpeed) {
            //    SwitchState(StateType.Wander);
            //}
        }
    }
}