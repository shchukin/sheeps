﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*
 * https://youtu.be/9Hq9rf0XgrI?t=110
 * https://youtu.be/Cm_OTNAjWvg?t=537 
 * https://youtu.be/Cm_OTNAjWvg?t=593
*/



/*
public class SheepStatesBehaviour : MonoBehaviour
{
    [HideInInspector] public SteeringLocomotion steering;
    [HideInInspector] public Rigidbody body;
    public Transform herder;
    public Transform dog;
    public float minHerderDistance = 4f;
    public float minDogDistance = 3f;
    public float rotSpeed = 180f;
    public Vector3 desiredPos;
    public Vector3 desiredDirection;
    public Quaternion desiredRot;
    public bool atDeadlock = false;

    Dictionary<State, SheepState> states;
    SheepState state;


    void Awake() {
        steering = transform.GetComponent<SteeringLocomotion>();
        body = transform.GetComponent<Rigidbody>();

        states = new Dictionary<State, SheepState>();
        states.Add(State.Idle, new SheepStateIdle(this));
        states.Add(State.ChooseWanderPos, new ChooseWanerPos(this));
        states.Add(State.Turn, new SheepStateTurn(this));
        states.Add(State.Wander, new SheepStateWander(this));
        states.Add(State.Flee, new SheepStateFlee(this));
        state = states[State.Idle];
    }

    void Update() {
        state.DoIt(Time.deltaTime);
        steering.ApplyForces();
    }

    void LateUpdate() {
        currState = state.StateType;
    }

    public void SwitchState(State newStateType) {
        state.OnExit();
        state = states[newStateType];
        state.OnEnter();
    }
}


public class ChooseWanerPos : SheepState {
    LayerMask groundLayerMask;
    LayerMask obstacleLayerMask;

    public override State StateType => State.ChooseWanderPos;

    public ChooseWanerPos(SheepStatesBehaviour fsm) : base(fsm) {
        groundLayerMask = 1 << LayerMask.NameToLayer("Ground");
        obstacleLayerMask = 1 << LayerMask.NameToLayer("Obstacle");
    }

    public override void OnEnter() {
        fsm.steering.SetState(SteeringLocomotion.State.Idle);
    }

    public override void DoIt(float deltaTime) {
        // TODO check available destination, might be need to change it
        // TODO check danger

        Vector3 pos = wanderTarget(fsm.atDeadlock);
        Vector3 look = pos - fsm.transform.position;
        //if (Physics.Raycast(pos + Vector3.up * 0.1f, look.normalized, look.magnitude, obstacleLayerMask.value)) {
        RaycastHit hit;
        if (Physics.SphereCast(pos + Vector3.up * 0.1f, 0.5f, look.normalized, out hit, look.magnitude, obstacleLayerMask.value)) {
                // TODO посмотреть вокруг и выбрать общее направление от всех препятствий
                // или повернуться на рандомный угол
                Debug.Log("bad point, choose next");
            fsm.atDeadlock = true;
            return;
        }

        fsm.desiredPos = pos;
        fsm.atDeadlock = false;
        fsm.SwitchState(State.Wander);
    }

    Vector3 wanderTarget(bool goBack) {
        const float Y = 5f;
        const float halfSize = 22f;
        const float halfQuad = 1.5f;
        const float stepDist = 2.5f;
        float x = Random.Range(-halfQuad, halfQuad);
        float z = Random.Range(-halfQuad, halfQuad);

        Vector3 forward = fsm.transform.forward;
        if (goBack) {
            forward = forward * -1f;
        }
        Vector3 u = fsm.transform.position + (forward * stepDist);
        u.x = Mathf.Clamp(u.x + x, -halfSize, halfSize);
        u.z = Mathf.Clamp(u.z + z, -halfSize, halfSize);
        u.y = Y;

        RaycastHit hit;
        if (Physics.Raycast(u, Vector3.down, out hit, 10f, groundLayerMask.value)) {
            u = hit.point;
        }


        return u;
    }
}
*/





