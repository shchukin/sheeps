﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sheep {

    public class SheepStateWander : SheepState {
        Vector3 direction;
        Vector3 prevPos;
        float stupidTimer;
        float movingTimer;


        public override StateType Type => StateType.Wander;

        public SheepStateWander(SheepStateMashine fsm, SheepMemory memory) : base(fsm, memory) { }

        public override void OnEnter() {
            stupidTimer = 0;
            //movingTimer = Random.Range(0.6f, 1.8f);
            movingTimer = 1f;

            //destination = wanderTarget(false);
            //direction = fsm.desiredDirection;
            direction = nextDirection();
            mem.desiredWanderDirection = direction;
            // 1 write destination, like a desired direction + time (or nearest available point)
            // 1.1 if point unavailable, choose random direction and check points with step by 30 degree (to 270 degree)
            // 2 check angle between forward and direction
            // 3 if angle more then 45 degree in each side, go to Turn state

            //Quaternion rot = Quaternion.LookRotation(direction);
            //float angle = Quaternion.Angle(mem.rotation, rot);
            //if (Mathf.Abs(angle) > 30f) {
            //    mem.desiredRot = rot;
            //    //fsm.SwitchState(SheepStateType.Turn);
            //    SwitchState(StateType.Idle);
            //    Debug.Log("wall");
            //    return;
            //}


            // fsm.steering.SetState(SteeringLocomotion.State.Walk);
        }

        public override void DoIt(float deltaTime) {
            if (checkDanger()) {
                SwitchState(StateType.Flee);
                return;
            }

            movingTimer -= deltaTime;
            if (movingTimer <= 0) {
                //int t = Random.Range(0, 100);
                //if (t < 1) {
                //    SwitchState(StateType.Idle);
                //} else {
                //    mem.desiredDirection = nextDirection();
                //    SwitchState(StateType.Wander);
                //}
                SwitchState(StateType.Wander);
                return;
            }

            if (Vector3.Distance(prevPos, mem.position) < 0.1f) {
                stupidTimer += deltaTime;
                if (stupidTimer > 1f) {
                    // direction is blocked
                    //destination = wanderTarget(true);
                    //stupidTimer = 0;
                    mem.atDeadlock = true;
                    SwitchState(StateType.Wander);
                    return;
                }
            } else {
                prevPos = mem.position;
                stupidTimer = 0;
            }

            //fsm.steering.Seek(fsm.transform.position + direction * 10f);
        }

        Vector3 nextDirection() {
            float angle = Random.Range(-15f, 15f);
            Vector3 f = mem.forward;
            f.y = 0;
            f = Quaternion.Euler(0, angle, 0) * f;
            return f.normalized;
        }

        public override void OnDrawGizmos() {
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(mem.position + Vector3.up, direction * 2f);
        }
    }


}
