﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Sheep {

    public class SheepController : MonoBehaviour {
        static List<SheepController> herd = null;
        static Transform herder = null;
        static Transform dog = null;

        public float maxForce = 5f;
        public float maxWalkSpeed = 1f;
        public float maxRunSpeed = 3f;
        public float minHerderWalkDistance = 4f;
        public float minDogWalkDistance = 3f;
        public float minHerderRunDistance = 2f;
        public float minDogRunDistance = 1.5f;
        public float minObstacleDistance = 3f;

        public float herdMaxDistance = 5f;
        public float herdBestDistance = 2f;
        public float herdGapDistance = 0.5f;

        [Range(0f, 5f)] public float dangerForceShare = 3f;
        [Range(0f, 5f)] public float obstacleForceShare = 1f;
        [Range(0f, 5f)] public float herdForceShare = 0.7f;
        [Range(0f, 5f)] public float wanderForceShare = 0.35f;
        [Range(0f, 5f)] public float finishForceShare = 4f;

        int obstacleLayer;
        int finishLayer;
        List<Collider> obstacles;
        Rigidbody body;

        bool isRun = false;
        Vector3 position => transform.position;
        Quaternion rotation => transform.rotation;
        Vector3 velocity => body.velocity;
        float maxSpeed { get { return isRun ? maxRunSpeed : maxWalkSpeed; } }
        float herderDistance;
        float dogDistance;

        float wanderTimer = 0;
        float wanderTimeout = 1f;
        Vector3 wanderTarget;

        Transform finishTarget = null;

        void Start() {
            if (herd == null) {
                herd = new List<SheepController>();
            }
            herd.Add(this);

            if (herder == null) {
                herder = GameObject.FindWithTag("Herder").transform;
            }

            if (dog == null) {
                dog = GameObject.FindWithTag("HerderDog").transform;
            }

            finishLayer = LayerMask.NameToLayer("Finish");
            obstacleLayer = LayerMask.NameToLayer("Obstacle");
            obstacles = new List<Collider>();

            body = GetComponent<Rigidbody>();
        }

        void OnDestroy() {
            if (herd != null) {
                herd.Remove(this);
                if (herd.Count == 0) {
                    herd = null;
                }
            }
        }

        void Update() {
            float deltaTime = Time.deltaTime;
            if (deltaTime == 0) {
                return;
            }

            isRun = checkDanger();

            Vector3 steeringForce = CalculateForce(deltaTime);
            UpdateLocomotion(steeringForce, deltaTime);
        }

        bool checkDanger() {
            herderDistance = Vector3.Distance(herder.position, position);
            dogDistance = Vector3.Distance(dog.position, position);
            return ((herderDistance < minHerderRunDistance) || (dogDistance < minDogRunDistance));
        }

        void UpdateLocomotion(Vector3 steeringForce, float deltaTime) {
            body.velocity = Vector3.ClampMagnitude(body.velocity, maxSpeed);
            steeringForce = Vector3.ClampMagnitude(steeringForce, maxForce);
            if (steeringForce.sqrMagnitude == 0 && body.velocity.sqrMagnitude > 0) {
                // drag
                float forceVal = Mathf.Lerp(0, -maxForce * 0.1f, body.velocity.magnitude / maxSpeed);
                steeringForce = body.velocity.normalized * forceVal;
                body.AddForce(steeringForce, ForceMode.Acceleration);
                return;
            }

            // TODO check turn back
            body.AddForce(steeringForce, ForceMode.Acceleration);

            // rotation
            Vector3 look = (body.velocity + steeringForce * deltaTime).normalized;
            look.y = 0;
            if (look.sqrMagnitude > 0.01) {
                Quaternion rot = Quaternion.LookRotation(look, Vector3.up);
                body.rotation = Quaternion.Slerp(body.rotation, rot, deltaTime * 10f);
            }
        }

        #region Obstacle vision
        private void OnTriggerEnter(Collider other) {
            if (other.gameObject.layer == obstacleLayer) {
                obstacles.Add(other);
            } else if (other.gameObject.layer == finishLayer) {
                finishTarget = other.transform;
            }
        }

        private void OnTriggerExit(Collider other) {
            if (other.gameObject.layer == obstacleLayer) {
                obstacles.Remove(other);
            }
        }
        #endregion

        Vector3 CalculateForce(float deltaTime) {
            Vector3 herderForce = calcFleeForce(herder.position, minHerderWalkDistance, minHerderRunDistance);
            Vector3 dogForce = calcFleeForce(dog.position, minDogWalkDistance, minDogRunDistance);

            Vector3 dangerForce = (herderForce + dogForce) * 0.5f;
            Vector3 obstaclesForce = calcObstaclesForce();
            Vector3 herdForce = calcHerdForce();

            Vector3 wanderForce = Vector3.zero;
            if (dangerForce.sqrMagnitude == 0) {
                wanderForce = calcWanderForce(deltaTime);
            } else {
                wanderTimer = wanderTimeout; // for choose next target
            }

            Vector3 finishForce = Vector3.zero;
            if (finishTarget != null) {
                if (Vector3.Distance(transform.position, finishTarget.position) > 1f) {
                    finishForce = Arrive(finishTarget.position, maxSpeed, deltaTime);
                }
            }

            Vector3 steeringForce = 
                dangerForce * dangerForceShare + 
                obstaclesForce * obstacleForceShare +
                herdForce * herdForceShare + 
                wanderForce * wanderForceShare + 
                finishForce * finishForceShare;
            return steeringForce;
        }

        Vector3 calcFleeForce(Vector3 target, float maxDangerDistance, float limitDangerDistance) {
            Vector3 desiredVel = position - target;
            float d = desiredVel.magnitude;
            if (d < maxDangerDistance) {
                Vector3 force = Flee(target, maxSpeed);
                float forceLimit;
                if (d <= limitDangerDistance) {
                    forceLimit = maxForce;
                } else {
                    float t = (d - limitDangerDistance) / (maxDangerDistance - limitDangerDistance);
                    forceLimit = Mathf.Lerp(maxForce, 0, t); // TODO заменить либо на квадрат расстояния, либо на анимационную кривую
                }
                force = Vector3.ClampMagnitude(force, forceLimit);
                return force;
            }
            return Vector3.zero;
        }

        /*
        Vector3 calcFleeForce(Vector3 target, float dangerDistance) {
            Vector3 desiredVel = position - target;
            float d = desiredVel.magnitude;
            if (d < dangerDistance) {
                Vector3 force = Flee(target, maxSpeed);
                float t = d / dangerDistance;
                float forceVal = Mathf.Lerp(maxForce, 0, t); // TODO заменить либо на квадрат расстояния, либо на анимационную кривую
                force = Vector3.ClampMagnitude(force, forceVal);
                return force;
            }
            return Vector3.zero;
        }
        */

        Vector3 calcObstaclesForce() {
            Vector3 totalForce = Vector3.zero;
            float counter = 0;
            foreach (Collider collider in obstacles) {
                Vector3 closest = collider.ClosestPoint(transform.position);
                Vector3 force = calcFleeForce(closest, minObstacleDistance, 0f);
                if (force.sqrMagnitude > 0) {
                    totalForce += force;
                    counter += 1f;
                }
            }
            if (counter > 1f) {
                totalForce /= counter;
            }
            totalForce = Vector3.ClampMagnitude(totalForce, maxForce);
            return totalForce;
        }

        Vector3 calcHerdForce() {
            Vector3 totalForce = Vector3.zero;

            int counter = 0;
            foreach (SheepController sheep in herd) {
                if (sheep == this) {
                    continue;
                }

                Vector3 v = transform.position - sheep.transform.position;
                float d = v.magnitude;
                if (d > herdMaxDistance) {
                    continue;
                }

                float k = (d - herdBestDistance);

                if (k > herdGapDistance) {
                    float t = k / (herdMaxDistance - (herdBestDistance + herdGapDistance));
                    //if (t < 0.1f) {
                    //    continue;
                    //}
                    float speed = Mathf.Lerp(0, maxSpeed, t);
                    Vector3 force = Seek(sheep.transform.position, speed);
                    totalForce += force;
                    counter++;
                } else if (k < -herdGapDistance) {
                    float t = -k / (herdBestDistance - herdGapDistance);
                    //if (t < 0.1f) {
                    //    continue;
                    //}
                    float speed = Mathf.Lerp(0, maxSpeed, t);
                    Vector3 force = Flee(sheep.transform.position, speed);
                    totalForce += force;
                    counter++;
                }
            }

            const int herdMinCount = 2;
            if (counter < herdMinCount) {
                totalForce = Vector3.zero;
            } else {
                totalForce /= counter;
            }

            return totalForce;
        }

        Vector3 calcWanderForce(float deltaTime) {
            if (wanderTimer >= wanderTimeout) {
                wanderTimer -= wanderTimeout;

                float angle = Random.Range(-30f, 30f);
                Vector3 dir = transform.forward;
                dir.y = 0; // 
                dir = Quaternion.Euler(0, angle, 0) * dir;
                wanderTarget = dir * maxSpeed; // * (wanderTimeout + 1f);
            }

            wanderTimer += deltaTime;
            Vector3 force = Seek(transform.position + wanderTarget, maxSpeed);
            return force;
        }

        #region Steering behaviour
        Vector3 Seek(Vector3 target, float maxSpeed) {
            Vector3 desiredVel = (target - position).normalized * maxSpeed;
            Vector3 steering = desiredVel - body.velocity;
            steering.y = 0;
            steering = steering.normalized * maxForce;
            return steering;
        }

        Vector3 Flee(Vector3 target, float maxSpeed) {
            return -Seek(target, maxSpeed);
        }

        Vector3 Arrive(Vector3 target, float maxSpeed, float deltaTime) {
            Vector3 desiredVel = Vector3.ClampMagnitude(target - position, maxSpeed);
            Vector3 steering = desiredVel - body.velocity;
            steering.y = 0;
            steering = Vector3.ClampMagnitude(steering / deltaTime, maxForce);
            return steering;
        }
        #endregion

        #region Gizmos
        private void OnDrawGizmos() {
            Gizmos.color = Color.red;
            if (herder != null && Vector3.Distance(transform.position, herder.position) < minHerderWalkDistance) {
                Gizmos.DrawLine(transform.position, herder.position);
            }
            if (dog != null && Vector3.Distance(transform.position, dog.position) < minDogWalkDistance) {
                Gizmos.DrawLine(transform.position, dog.position);
            }

            OnDrawGizmosHeard();
            OnDrawGizmosObstacles();

            if (body != null) {
                Gizmos.color = Color.green;
                Gizmos.DrawRay(position, body.velocity);
            }
        }

        void OnDrawGizmosHeard() {
            if (herd == null) {
                return;
            }

            Gizmos.color = Color.cyan;
            foreach (var sheep in herd) {
                if (sheep == this) {
                    continue;
                }

                if (Vector3.Distance(transform.position, sheep.transform.position) < herdMaxDistance) {
                    Gizmos.DrawLine(transform.position, sheep.transform.position);
                }
            }
        }

        void OnDrawGizmosObstacles() {
            if (obstacles == null) {
                return;
            }

            Gizmos.color = Color.magenta;
            foreach (var t in obstacles) {
                Vector3 closest = t.ClosestPoint(transform.position);
                Gizmos.DrawLine(transform.position, closest);
            }
        }
        #endregion
    }

} // namespace