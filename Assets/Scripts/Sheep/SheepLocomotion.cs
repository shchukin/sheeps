﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Sheep {

    public class SheepLocomotion {
        Transform mesh;
        Rigidbody body;
        SheepMemory memory;
        Vector3 prevPos;
        float timer = 0;
        float recalculateTimeout = 0.2f;
        LayerMask groundMask;


        public SheepLocomotion(Rigidbody body, SheepMemory memory, Transform mesh) {
            this.body = body;
            this.memory = memory;
            this.mesh = mesh;

            prevPos = body.position;
            groundMask = 1 << LayerMask.NameToLayer("Ground");
        }

        public void Update(float deltaTime) {
            // TODO check state changing + object count

            //timer += deltaTime;
            //if (timer < recalculateTimeout) {
            //    return;
            //}
            //timer -= recalculateTimeout;

            Vector3 steering = Vector3.zero;
            if (Vector3.Distance(memory.herder.position, memory.position) < memory.minHerderDistance) {
                steering += Flee(memory.herder.position);
            }
            if (Vector3.Distance(memory.dog.position, memory.position) < memory.minDogDistance) {
                steering += Flee(memory.dog.position);
            }

            if (steering.sqrMagnitude == 0) {
                steering += Seek(memory.position + memory.desiredWanderDirection);
            }

            foreach (var collider in memory.obstacles) {
                Vector3 closest = collider.ClosestPoint(memory.position);
                steering += Flee(closest);
            }


            body.velocity = Vector3.ClampMagnitude(body.velocity, memory.maxSpeed); ;
            steering = Vector3.ClampMagnitude(steering, memory.maxForce);
            body.AddForce(steering, ForceMode.Acceleration);

            // rotation
            Vector3 look = (body.velocity + steering * deltaTime).normalized;
            look.y = 0;
            if (look.sqrMagnitude > 0) {
                Quaternion rot = Quaternion.LookRotation(look, Vector3.up);
                //float angle = Quaternion.Angle(transform.rotation, rot);
                //mesh.rotation = Quaternion.Slerp(mesh.rotation, rot, deltaTime * 10f);
                body.rotation = Quaternion.Slerp(body.rotation, rot, deltaTime * 10f);
            }
        }

        Vector3 Seek(Vector3 target) {
            Vector3 desiredVel = (target - memory.position).normalized * memory.maxSpeed;
            Vector3 steering = desiredVel - body.velocity;
            steering.y = 0;
            steering = steering.normalized * memory.maxForce;
            return steering;
        }

        Vector3 Flee(Vector3 target) {
            return -Seek(target);
        }

        Vector3 Arrive(Vector3 target, float deltaTime) {
            Vector3 desiredVel = Vector3.ClampMagnitude(target - memory.position, memory.maxSpeed);
            Vector3 steering = desiredVel - body.velocity;
            steering.y = 0;
            steering = Vector3.ClampMagnitude(steering / deltaTime, memory.maxForce);
            return steering;
        }

        //void updateRotation() {
        //    Vector3 d = body.position - prevPos;
        //    d.y = 0;
        //    if (d.sqrMagnitude > 0.05) {
        //        Quaternion rot = Quaternion.LookRotation(d);
        //        body.rotation = rot;
        //    }
        //}

        public void OnDrawGizmos() {
        }
    }

}