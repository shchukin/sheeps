﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sheep {

    public class SheepStateFlee : SheepState {
        public override StateType Type => StateType.Flee;

        public SheepStateFlee(SheepStateMashine fsm, SheepMemory memory) : base(fsm, memory) { }

        public override void DoIt(float deltaTime) {
            bool isRunning = false;

            if (Vector3.Distance(mem.herder.position, mem.position) <= mem.minHerderDistance) {
                isRunning = true;
                //fsm.steering.SetState(SteeringLocomotion.State.Run);
                //fsm.steering.Flee(fsm.herder.position);
            }

            if (Vector3.Distance(mem.dog.position, mem.position) <= mem.minDogDistance) {
                isRunning = true;
                //fsm.steering.SetState(SteeringLocomotion.State.Run);
                //fsm.steering.Flee(fsm.dog.position);
            }

            if (!isRunning) {
                SwitchState(StateType.Wander);
            }
        }
    }

}
