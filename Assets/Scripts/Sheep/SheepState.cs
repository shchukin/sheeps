﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sheep {

    public enum StateType {
        Idle,
        //ChooseWanderPos,
        Wander,
        Flee,
        Turn,
        //FastTurn
    }


    public interface SheepStateMashine {
        void SwitchState(StateType newStateType);
    }


    public class SheepMemory {
        public float maxForce = 1f;
        public float maxSpeed = 1f;

        public Transform herder;
        public Transform dog;
        public List<SheepController> herd;
        public List<Collider> obstacles;

        public float minHerderDistance => 4f;
        public float maxHerdDistance => 5f;
        public float minDogDistance => 3f;
        public float rotSpeed => 180f;

        public Vector3 desiredPos;
        public Vector3 desiredWanderDirection;
        public Quaternion desiredRot;
        public bool atDeadlock = false;

        public Vector3 dangerForce;
        public Vector3 obstaclesForce;
        public Vector3 herdForce;

        public Transform transform;
        public Vector3 forward => transform.forward;
        public Vector3 position => transform.position;
        public Quaternion rotation => transform.rotation;

        public SheepMemory(Transform transform) {
            this.transform = transform;
        }
    }


    public abstract class SheepState {
        private SheepStateMashine fsm;
        protected SheepMemory mem;

        protected SheepState(SheepStateMashine fsm, SheepMemory memory) {
            this.fsm = fsm;
            this.mem = memory;
        }

        public abstract StateType Type { get; }
        public virtual void OnEnter() { }
        public virtual void OnExit() { }
        public virtual void DoIt(float deltaTime) { }
        public virtual void OnDrawGizmos() { }

        protected bool checkDanger() {
            if (Vector3.Distance(mem.herder.position, mem.position) <= mem.minHerderDistance) {
                return true;
            }

            if (Vector3.Distance(mem.dog.position, mem.position) <= mem.minDogDistance) {
                return true;
            }

            return false;
        }

        protected void SwitchState(StateType newStateType) {
            fsm.SwitchState(newStateType);
        }
    }
}