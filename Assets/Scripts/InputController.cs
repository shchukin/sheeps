﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface InputController
{
    Vector2 MoveDirection();
}
