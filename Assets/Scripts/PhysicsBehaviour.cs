﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsBehaviour : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        float t = Mathf.Min(0.05f, Time.deltaTime);
        //Physics.Simulate(Time.deltaTime);
        Physics.Simulate(t);
        //Physics.Simulate(0.1f);
    }
}
