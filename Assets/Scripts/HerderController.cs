﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerderController : MonoBehaviour
{
    public Joystick joystick;
    public float maxSpeed = 4f;
    public float maxForce = 16f;

    public DogController dog;
    public Vector2 dogDistance;
    public ParticleSystem dogMindParticles;
    //public float shoutRate = 0.5f;

    Locomotion locomotion;
    //float nextFireTime = 0;

    void Start() {
        Rigidbody body = GetComponent<Rigidbody>();

        IUserController userController = new JoystickController(joystick);
        locomotion = new Locomotion(maxSpeed, maxForce, body, userController);
    }

    void Update()
    {
        updateDogState();
        locomotion.Update();
        
        //updateShout();
    }

    void updateDogState() {
        float d = Vector3.Distance(transform.position, dog.transform.position);

        if (d < dogDistance.x) {
            playDogMind(false);
        } else if (d > dogDistance.y) {
            playDogMind(false);
            // TODO call dog
        } else {
            playDogMind(true);
        }
    }

    void playDogMind(bool turnOn) {
        if (turnOn && !dogMindParticles.isPlaying) {
            dogMindParticles.Play();
        } else if (!turnOn && dogMindParticles.isPlaying) {
            dogMindParticles.Stop();
        }
    }

    void updateShout() {
        //if (!inputController.Fire()) {
        //    return;
        //}

        //float t = Time.realtimeSinceStartup;
        //if (t < nextFireTime) {
        //    return;
        //}
        //nextFireTime = t + 1f / shoutRate;

        //Debug.Log("SHOUT");
    }
}
