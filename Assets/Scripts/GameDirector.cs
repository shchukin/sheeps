﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDirector : MonoBehaviour
{
    public Vector2 dogDistance;
    public UIDogDistance uiDogDistance;

    HerderController herder;
    DogController dog;
    ParticleSystem dogMindParticles;
    bool isPlaying = false;

    void Start() {
        herder = GameObject.FindWithTag("Herder").transform.GetComponent<HerderController>();
        dog = GameObject.FindWithTag("HerderDog").transform.GetComponent<DogController>();
        dogMindParticles = herder.transform.GetComponentInChildren<ParticleSystem>();
        Debug.Log(dogMindParticles);
    }

    void Update() {
        updateDogState();
    }

    void updateDogState() {
        float d = Vector3.Distance(herder.transform.position, dog.transform.position);

        if (dog.GetState() == DogController.State.FreeRun) {
            if (d < dogDistance.x) {
                playDogMind(false);
                uiDogDistance.SetValue(0);
            } else if (d > dogDistance.y) {
                playDogMind(false);
                uiDogDistance.SetValue(1);
                dog.SwitchState(DogController.State.GoHerder);
            } else {
                playDogMind(true);
                float val = (d - dogDistance.x) / (dogDistance.y - dogDistance.x);
                uiDogDistance.SetValue(val);
            }
        } else if (d < 2f) {
            dog.SwitchState(DogController.State.FreeRun);
        }
    }

    void playDogMind(bool turnOn) {
        //Debug.Log("is playing: " + dogMindParticles.isPlaying);
        //if (turnOn && !dogMindParticles.isPlaying) {
        if (turnOn && !isPlaying) {
            isPlaying = true;
            dogMindParticles.Play();
            Debug.Log("Play, is playing: " + dogMindParticles.isPlaying + ", " + dogMindParticles.time);
        //} else if (!turnOn && dogMindParticles.isPlaying) {
        } else if (!turnOn && isPlaying) {
            isPlaying = false;
            dogMindParticles.Stop();
            Debug.Log("Stop");
        }
    }

    void OnDrawGizmos() {
        if (herder == null || dog == null) {
            return;
        }

        float d = Vector3.Distance(herder.transform.position, dog.transform.position);
        if (d > dogDistance.y) {
            Gizmos.color = Color.red;
        } else if (d > dogDistance.x) {
            Gizmos.color = Color.yellow;
        } else {
            return;
        }

        Gizmos.DrawLine(herder.transform.position, dog.transform.position);
    }
}
