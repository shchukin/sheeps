﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDogDistance : MonoBehaviour
{
    public RawImage dog;
    public Image slider;
    public Color startWarnColor;
    public Color endWarnColor;

    Color defaultDogColor;
    float value = -1;

    void Start() {
        defaultDogColor = dog.color;
        SetValue(0);    
    }

    public void SetValue(float value) {
        if (this.value == value) {
            return;
        }
        this.value = value;

        if (value == 0) {
            dog.color = defaultDogColor;
            slider.fillAmount = 0;
        } else {
            Color color = Color.Lerp(startWarnColor, endWarnColor, value);
            dog.color = color;
            slider.color = color;
            slider.fillAmount = value;
        }
    }
}
