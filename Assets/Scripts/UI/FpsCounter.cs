﻿using UnityEngine;
using UnityEngine.UI;
 
public class FpsCounter : MonoBehaviour
{
    public Text _fpsText;
    public float _hudRefreshRate = 1f;
 
    private float _timer;
    private int frameCounter;
 
    private void Update()
    {
        frameCounter++;
        if (Time.unscaledTime > _timer)
        {
            //int fps = (int)(1f / Time.unscaledDeltaTime);
            int fps = (int)(frameCounter / _hudRefreshRate);
            frameCounter = 0;
            _fpsText.text = "FPS: " + fps;
            _timer = Time.unscaledTime + _hudRefreshRate;
        }
    }
}