﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IUserController {
    Vector2 Direction { get; }
}


public class JoystickController : IUserController {
    Joystick joystick;

    public virtual Vector2 Direction => joystick.Direction;

    public JoystickController(Joystick joystick) {
        this.joystick = joystick;
    }
}


public class Locomotion 
{
    float maxSpeed;
    float maxForce;
    Rigidbody body;
    IUserController userController;

    public Locomotion(float maxSpeed, float maxForce, Rigidbody body, IUserController userController) {
        this.maxSpeed = maxSpeed;
        this.maxForce = maxForce;
        this.body = body;
        this.userController = userController;
    }

    public void Update()
    {
        body.velocity = Vector3.ClampMagnitude(body.velocity, maxSpeed);

        Vector2 v = userController.Direction;
        if (v == Vector2.zero) {
            // drag
            float forceVal = Mathf.Lerp(0, -maxForce, body.velocity.magnitude / maxSpeed);
            Vector3 steeringForce = body.velocity.normalized * forceVal;
            body.AddForce(steeringForce, ForceMode.Acceleration);
            return;
        }

        Vector3 forceDirection = new Vector3(v.x, 0, v.y).normalized;
        body.AddForce(forceDirection * maxForce, ForceMode.Acceleration);

        Vector3 vel = body.velocity;
        vel.y = 0;
        if (vel.sqrMagnitude > 0.01f) {
            body.rotation = Quaternion.LookRotation(vel.normalized, Vector3.up);
        }
        //body.rotation = Quaternion.LookRotation(forceDirection, Vector3.up);
    }
}
